package com.abarna.graphql.springbootgraphqlstarterexample.resources;

import com.abarna.graphql.springbootgraphqlstarterexample.services.GraphQlService;
import graphql.ExecutionResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/teams")
public class TeamResource {

    @Autowired
    private GraphQlService graphQlService;

    @PostMapping
    public ResponseEntity<Object> getAllTeams(@RequestBody String query) {
        ExecutionResult executionResult = graphQlService.getGraphQL().execute(query);
        return new ResponseEntity<>(executionResult, HttpStatus.OK);
    }
}
