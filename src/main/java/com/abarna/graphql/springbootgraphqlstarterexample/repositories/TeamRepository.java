package com.abarna.graphql.springbootgraphqlstarterexample.repositories;

import com.abarna.graphql.springbootgraphqlstarterexample.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamRepository extends JpaRepository<Team, String> {
}
