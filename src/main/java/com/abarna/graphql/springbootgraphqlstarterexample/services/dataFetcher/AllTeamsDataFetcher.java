package com.abarna.graphql.springbootgraphqlstarterexample.services.dataFetcher;

import com.abarna.graphql.springbootgraphqlstarterexample.model.Team;
import com.abarna.graphql.springbootgraphqlstarterexample.repositories.TeamRepository;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AllTeamsDataFetcher implements DataFetcher<List<Team>> {

    @Autowired
    private TeamRepository teamRepository;

    @Override
    public List<Team> get(DataFetchingEnvironment dataFetchingEnvironment) {
        return teamRepository.findAll();
    }
}
