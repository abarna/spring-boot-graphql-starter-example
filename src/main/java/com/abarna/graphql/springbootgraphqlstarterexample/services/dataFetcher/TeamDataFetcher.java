package com.abarna.graphql.springbootgraphqlstarterexample.services.dataFetcher;

import com.abarna.graphql.springbootgraphqlstarterexample.model.Team;
import com.abarna.graphql.springbootgraphqlstarterexample.repositories.TeamRepository;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TeamDataFetcher implements DataFetcher<Team> {

    @Autowired
    private TeamRepository teamRepository;

    @Override
    public Team get(DataFetchingEnvironment dataFetchingEnvironment) {
        String teamName = dataFetchingEnvironment.getArgument("teamName");
        return teamRepository.getOne(teamName);
    }
}
