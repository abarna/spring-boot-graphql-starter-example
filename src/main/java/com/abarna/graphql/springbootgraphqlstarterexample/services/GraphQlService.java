package com.abarna.graphql.springbootgraphqlstarterexample.services;

import com.abarna.graphql.springbootgraphqlstarterexample.model.Team;
import com.abarna.graphql.springbootgraphqlstarterexample.repositories.TeamRepository;
import com.abarna.graphql.springbootgraphqlstarterexample.services.dataFetcher.AllTeamsDataFetcher;
import com.abarna.graphql.springbootgraphqlstarterexample.services.dataFetcher.TeamDataFetcher;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

import static com.google.common.collect.Lists.newArrayList;

@Service
public class GraphQlService {

    private GraphQL graphQL;
    @Autowired
    private AllTeamsDataFetcher allTeamsDataFetcher;
    @Autowired
    private TeamDataFetcher teamDataFetcher;
    @Autowired
    private TeamRepository teamRepository;

    @PostConstruct
    private void loadSchema() {
        loadDataInDB();
        //getting the schema
        String schemaText = "schema {\n" +
                "    query:Query\n" +
                "}\n" +
                "\n" +
                "type Query{\n" +
                "    allTeams : [Team]\n" +
                "    team(teamName: String): Team\n" +
                "}\n" +
                "\n" +
                "type Team{\n" +
                "    teamName: String\n" +
                "    players: [String]\n" +
                "    foundedDate: String\n" +
                "}";
        //parsing schema
        TypeDefinitionRegistry typeDefinitionRegistry = new SchemaParser().parse(schemaText);
        RuntimeWiring wiring = buildRuntimeWiring();
        GraphQLSchema schema = new SchemaGenerator().makeExecutableSchema(typeDefinitionRegistry, wiring);
        graphQL = GraphQL.newGraphQL(schema).build();
    }

    private void loadDataInDB() {
        newArrayList(
                new Team("Real Madrid", new String[]{"Bale, Asensio"}, "1902"),
                new Team("Barcelona", new String[]{"Messi, Suarez"}, "1899"),
                new Team("Atletico Madrid", new String[]{"Griezmann, Costa"}, "1903")
        ).forEach(team -> teamRepository.save(team));
    }

    private RuntimeWiring buildRuntimeWiring() {
        return RuntimeWiring.newRuntimeWiring()
                .type("Query", typeWiring ->
                        typeWiring
                                .dataFetcher("allTeams", allTeamsDataFetcher)
                                .dataFetcher("team", teamDataFetcher)
                )
                .build();
    }

    public GraphQL getGraphQL() {
        return graphQL;
    }
}
