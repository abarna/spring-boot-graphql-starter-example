package com.abarna.graphql.springbootgraphqlstarterexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootGraphqlStarterExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootGraphqlStarterExampleApplication.class, args);
	}
}
